## [1.1.7](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.6...1.1.7) (2024-05-31)


### Bug Fixes

* **deps:** update poetry dependencies ([43dbee2](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/43dbee2da3ef296ae72a03d355ed0ba7a892562f))

## [1.1.6](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.5...1.1.6) (2024-05-27)


### Bug Fixes

* don't fail when Git repo has no commit ([8ae334d](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/8ae334d65e7d5ab07be6b84843262aeef69318d1))
* use subgroups API instead of descendant_groups ([ac5fad3](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/ac5fad33a36e1e39917ffb75310c5c629442463c)), closes [#2](https://gitlab.com/to-be-continuous/tools/gitlab-cp/issues/2)

## [1.1.5](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.4...1.1.5) (2024-04-30)


### Bug Fixes

* add more logs in case of ppty update failure ([cadb6dd](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/cadb6dd736cb8a0e8f825d9788c921c62f6a22dc))
* don't update avatar if src group/project is not public ([7140a4a](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/7140a4a1c272bb27ebd61ce7b98dce4aa589b5df))

## [1.1.4](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.3...1.1.4) (2024-04-30)


### Bug Fixes

* wrong and operator ([2660fbc](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/2660fbc0f3f0db0cd3105c2409acb586cf2b2e2b))

## [1.1.3](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.2...1.1.3) (2024-04-30)


### Bug Fixes

* parent group is None for a 1-depth path ([53c1558](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/53c1558d1666d11fb2a3608e00a19fc459464474))

## [1.1.2](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.1...1.1.2) (2024-04-29)


### Bug Fixes

* default exclude is empty list ([08e8a92](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/08e8a92c0223b1e9cbc9ea8deea98febfd7fecc6))

## [1.1.1](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.0...1.1.1) (2024-04-29)


### Bug Fixes

* add Git tool to docker image ([09b8435](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/09b843530ee3af14ae4ab1876f1d1c502e2f245c))

# [1.1.0](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.0.0...1.1.0) (2024-04-27)


### Features

* add cache dir argument ([d3d6c0a](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/d3d6c0a32c681212a8441285400205f99b6ce2a1))
* force update release only on latest ([69a6660](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/69a6660524b1486908f98ef9b44ae8ddcb1cfce8))
* reliable dry-run ([a30228c](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/a30228ca74aeaa3cef9c18e73dcce15b4f91ebfc))

# 1.0.0 (2024-04-07)


### Features

* initial commit ([8daeca6](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/8daeca679cf543308fe4e3d4bdcace4af593d06f))
